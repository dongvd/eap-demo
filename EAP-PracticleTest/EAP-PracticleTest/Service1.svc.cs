﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace EAP_PracticleTest
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        EmplpoyeeDataDataContext data = new EmplpoyeeDataDataContext();
        public bool AddEmployee(Employee newEmp)
        {
            try
            {
                data.Employees.InsertOnSubmit(newEmp);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<Employee> ListEmployee()
        {
            try
            {
                return (from emp in data.Employees select emp).ToList();
            }
            catch
            {
                return null;
            }
        }

        public Employee SearchEmployee(string DepartmentName)
        {
            try
            {
                return (from emp in data.Employees where emp.Department == DepartmentName select emp).Single();
            }
            catch
            {
                return null;
            }
        }
    }
}
