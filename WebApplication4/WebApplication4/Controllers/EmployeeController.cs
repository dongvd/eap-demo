﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication4.Controllers
{
    public class EmployeeController : Controller
    {
        ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();
        // GET: Employee
        public ActionResult Index()
        {
            return View(client.ListEmployee());
        }

        // GET: Search/DepartmentName
        public ActionResult Search(string DepartmentName)
        {
            return View();
        }

        // GET: Employee/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employee/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

       
    }
}
